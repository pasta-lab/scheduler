package arch

import (
	. "goa.design/model/dsl"
	"goa.design/model/expr"
)

// example: https://raw.githubusercontent.com/goadesign/model/main/examples/big_bank_plc/model/model.go

var _ = Design("Scheduler", "Scheduler is a service, that calls HTTP endpoints on programmable cron like schedules.", func() {
	Enterprise("Scheduler")
	Version("v0.0.0")

	var CronUser = Person("Cron User", "Scheduler service user, usually a programmer", func() {
		External()
		Uses("Scheduler", "Uses", Synchronous, func() {
			Tag("Relationship", "Synchronous")
		})
		Uses("Scheduler/API", "Uses", Synchronous, func() {
			Tag("Relationship", "Synchronous")
		})
		Tag("Element", "Person")
	})

	var TargetAPI = SoftwareSystem("Target API", "Configurable target HTTP API, to be called by crons, on schedule, can be anything the user configures", func() {
		Tag("Element", "Software System", "Existing System")
	})

	var Vault = SoftwareSystem("Vault", "Stores secrets and certificates", func() {
		Tag("Element", "Software System", "Existing System")
	})

	var (
		// Forward declaration so variables can be used to define views.
		Database      *expr.Container
		API           *expr.Container
		Worker        *expr.Container
		WorkerManager *expr.Container
		CronManager   *expr.Container
	)

	var SchedulerService = SoftwareSystem("Scheduler", "Interface for the user to manage it's crons and targets", func() {
		Uses(TargetAPI, "Uses", Synchronous, func() {
			Tag("Relationship", "Synchronous")
		})
		Uses(Vault, "Uses", Synchronous, func() {
			Tag("Relationship", "Synchronous")
		})
		Tag("Element", "Software System")

		Database = Container("Database", "Store all data related to Scheduler system", "PosgreSQL Database", func() {
			Tag("Element", "Container", "Database")
		})

		API = Container("API", "Interface for the Scheduler service via a JSON/HTTPS API.", "Golang", func() {
			Uses(Database, "Reads from and writes to", Synchronous, func() {
				Tag("Relationship", "Synchronous")
			})
			Tag("Element", "Container")

			Component("Auth Service", "Allows users to authenticate into the Scheduler service", "Golang", func() {
				Uses(Database, "", Synchronous, func() {
					Tag("Relationship", "Synchronous")
				})
				Tag("Element", "Component")
			})

			Component("Cron Service", "Manages crons data", "Golang", func() {
				Uses(Database, "", Synchronous, func() {
					Tag("Relationship", "Synchronous")
				})
				Tag("Element", "Component")
			})
		})

		Worker = Container("Worker", "Picks up tasks once created by CronManager, responsible for performing requests to the target APIs", "Golang", func() {
			Uses(TargetAPI, "Perform POST request to", Synchronous, func() {
				Tag("Relationship", "Synchronous")
			})
			Uses(Database, "Reads from and writes to", Synchronous, func() {
				Tag("Relationship", "Synchronous")
			})
			Tag("Element", "Container")
		})

		WorkerManager = Container("Worker Manager", "Monitors tasks for workers and move them back to be processed again in case of worker failures", "Golang", func() {
			Uses(Database, "Reads from and writes to", Synchronous, func() {
				Tag("Relationship", "Synchronous")
			})
			Tag("Element", "Container")
		})

		CronManager = Container("Cron Manager", "Responsible for holding crons and creating tasks for workers when at the configured times of crons", "Golang", func() {
			Uses(Database, "Reads from and writes to", Synchronous, func() {
				Tag("Relationship", "Synchronous")
			})
			Tag("Element", "Container")
		})
	})

	Views(func() {
		SystemLandscapeView("SystemLandscape", "The system landscape diagram for Scheduler.", func() {
			PaperSize(SizeA5Landscape)
			EnterpriseBoundaryVisible()
			AutoLayout(RankTopBottom)

			Add(CronUser, func() {
				//Coord(87, 643)
			})
			Add(Vault, func() {
			})
			Add(SchedulerService, func() {
			})
			Add(TargetAPI, func() {
			})
		})

		ContainerView("Scheduler", "Containers", "The container diagram for the Scheduler service.", func() {
			PaperSize(SizeA5Landscape)
			AutoLayout(RankBottomTop)

			Add(CronUser, func() {
				//Coord(1056, 24)
			})
			Add(API, func() {
			})
			Add(CronManager, func() {
			})
			Add(Worker, func() {
			})
			Add(WorkerManager, func() {
			})
			Add(Database, func() {
			})
		})

		ComponentView(API, "Components", "The component diagram for the API Application", func() {
			PaperSize(SizeA5Landscape)
			AutoLayout(RankBottomTop)

			Add("Auth Service", func() {
				//Coord(1925, 817)
			})
			Add("Cron Service", func() {
			})
			Add(Database, func() {
			})
		})

		Styles(func() {
			ElementStyle("Software System", func() {
				Background("#1168bd")
				Color("#ffffff")
			})
			ElementStyle("Container", func() {
				Background("#438dd5")
				Color("#ffffff")
			})
			ElementStyle("Component", func() {
				Background("#85bbf0")
				Color("#000000")
			})
			ElementStyle("Person", func() {
				Background("#08427b")
				Color("#ffffff")
				FontSize(22)
				Shape(ShapePerson)
			})
			ElementStyle("Existing System", func() {
				Background("#999999")
				Color("#ffffff")
			})
			ElementStyle("Bank Staff", func() {
				Background("#999999")
				Color("#ffffff")
			})
			ElementStyle("Web Browser", func() {
				Shape(ShapeWebBrowser)
			})
			ElementStyle("Mobile App", func() {
				Shape(ShapeMobileDeviceLandscape)
			})
			ElementStyle("Database", func() {
				Shape(ShapeCylinder)
			})
			ElementStyle("Failover", func() {
				Opacity(25)
			})
			RelationshipStyle("Failover", func() {
				Opacity(25)
			})
		})
	})
})
