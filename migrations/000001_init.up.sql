CREATE TABLE cron
(
    id         CHAR(4),
    job_name   VARCHAR(64),
    cron       VARCHAR(64) NOT NULL,
    created_at TIMESTAMP   NOT NULL,
    updated_at TIMESTAMP   NOT NULL,
    deleted_at TIMESTAMP DEFAULT NULL
);
ALTER TABLE cron
    ADD CONSTRAINT cron_pk PRIMARY KEY (id);

CREATE TABLE cron_execution_lock
(
    cron_id           CHAR(4),
    scheduler_created CHAR(4)   NOT NULL,
    status            CHAR(4)   NOT NULL,
    acquired_at       TIMESTAMP NOT NULL,
    created_at        TIMESTAMP NOT NULL,
    updated_at        TIMESTAMP NOT NULL,
    deleted_at        TIMESTAMP DEFAULT NULL
);
ALTER TABLE cron_execution_lock
    ADD CONSTRAINT cron_execution_lock_pk PRIMARY KEY (cron_id);
ALTER TABLE cron_execution_lock
    ADD CONSTRAINT cron_fkey FOREIGN KEY (cron_id) REFERENCES cron (id);

-- INSERT INTO cron VALUES ('aaaa', '1', '*/1 * * * *', NOW(), NOW());
-- INSERT INTO cron_execution_lock (cron_id, scheduler_created, status, acquired_at, created_at, updated_at) VALUES ('aaaa', 'date', 'done', NOW(), NOW(), NOW());
