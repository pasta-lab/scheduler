## Tools

Directory dedicated to installing tools used by the project, but not exactly a direct code dependency.

Installed tools should appear in the `../bin` directory, and if you're using `direnv` it should automatically be added to your `$PATH`.

### How to install a new dependency tool

1. `go get <url>`, so the dependency appears in `go.mod`
2. add the tool to `install-tools` in the `makefile`
3. import the tool in `tools.go`
4. run `go mod tidy`
