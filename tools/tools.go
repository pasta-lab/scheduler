package tools

import (
	_ "github.com/cespare/reflex"
	_ "github.com/golang-migrate/migrate/v4/cmd/migrate"
	_ "github.com/volatiletech/sqlboiler/v4"
	_ "github.com/volatiletech/sqlboiler/v4/drivers/sqlboiler-psql"
	_ "goa.design/model/cmd/mdl"
	_ "goa.design/model/cmd/stz"
)
