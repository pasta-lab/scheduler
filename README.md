# Scheduler

The main feature of this application is performing a configurable request, given a cronlike schedule.

We strive to reach these objectives, and could potentially create some SLA's on these objectives at a later time

- we'll trigger the request as close to the scheduled time as possible
- once the schedule is created, we'll guarantee at least one request is made, i.e. a failure in an instance of this system will eventually be made by other instances.
- this system can scale for a great amount of schedules.

## Decisions

1. any cron like software consumes some CPU and memory, so 1 single instance could not be capable of running all schedules, given a big enough amount of schedules
   - we could partially solve this by usage of partitioning, problem: some schedules might be more demanding
2. one of the objectives is to have 100% sure we make at least 1 request to the target API within a reasonable amount of time, but any instance could fail at any point in time
   - note that the target api should be able to handle multiple requests, caused by possible fails, on the scheduler end, one way to handle it is to include within the request some manner of request ID uniquely attached to this single schedule **trigger**
   - having some failover instances is a possible solution
     - I don't quite like this approach, because this instance would need some manner of coordination with a "master" instance, and in case of "master" failure, there would need to be an election of the failovers to become a new master
   - ideally all instances would be concurrent for the schedules trigger, i.e., there would be no "master" or "failover" instances, all would "compete" to trigger the scheduled request
     - this would mean more waste of resources, if all would perform the same job, only the job of 1 would be "fruitful"
     - maybe have a distributed lock?
       - https://redis.io/docs/reference/patterns/distributed-locks/
       - note that due to the nature of the target api to handle possibly multiple requests at the same time, a weak lock could be enough
3. define retry strategy for the request
   - can client configure it?
   - backoff? it will affect the time of the request
4. can we generalize somehow? i.e. run custom jobs (binaries)? or is making a HTTP request enough?
5. we should probably think of some manner of traceability/auditability/monitoring of triggers sooner rather than later

## Cron libs

- https://github.com/mileusna/crontab: unusable, can't remove/edit crons
- https://github.com/robfig/cron: cloud sync uses this

## Locking POC

we have to implement distributed locking for:
- workers should pick up jobs correctly, jobs will likely be a row in a jobs table. I.e., a job has to be picked up by a single worker, and picking jobs has to be made concurrently by workers, it can't be serial.
- triggering jobs has to be made concurrently too. I.e, the cron managers of more than 1 instance has to generate a single job, for 1 trigger

Using postgres, we have some features that can be used to acquire locks:
- pg_try_advisory_lock: function that receives an **int**
- `FOR UPDATE` statetement: we can select rows for update, and it'll be locked for others transactions to access the locked rows. Note that it may blockk other select statetemens
- `SKIP LOCKED` statement: this is a way to select and skip locked rows

sources:
- https://www.2ndquadrant.com/en/blog/what-is-select-skip-locked-for-in-postgresql-9-5/
- https://www.postgresql.org/docs/current/explicit-locking.html

I'd prefer to use `pg_try_advisory_lock` if we use int ids for jobs, however it's not our case, so we're kind of stuck with `SELECT FOR UPDATE SKIP LOCKED` statements.

So that brings the question about what table we'll lock, we can't lock the cron table, because it's possible the table will be used for loading resources into mem, by other instances, and it'll possibly be locked.

A possible solution is creating an auxiliary table like `cron_execution_lock`, which references a single cron, and all of the instances will compete to successfully lock the respective cron exectution lock. The instance that acquires the lock will be able to create the respective cron job trigger.

From the creation of the job, the workers can use the `FOR UPDATE SKIP LOCKED` to correctly acquire a new job that was not acquired by any other worker.

Can we create the job from the schedule controller? by locking the job creation, is there a way for all the Scheduler instances to coordinate a common id to create the job?

### Creating jobs

Scenario suppose a user created a schedule, then, we want to ensure it's later processed by the workers, so we need to at leat create the job in the database.

In this scenario we need to have multiple "schedule controllers" that can create the job, and they will compete to create it, and they should create 1 and only 1 job.

Next we discuss several possibilities. Remember that in any case, before creating the job, we have to respect a configuration for the time tolerance for calling the target api.

#### Multiple instances happy path

1. instance 1 acquires lock and writes the job
2. instance 2 fails to acquire lock, and monitors the job creation, soon finds out the job was created successfully, then drops the monitoring

#### Multiple instances locking instance fails

1. instance 1 acquires lock, but fails while holding the lock. This instance should release the lock eventually, possibly by implementing context cancellation with some manner of deadline. (see viability of this, check: `SET lock_timeout TO '1s'`)
2. instance 2 fails to acquire lock, and monitors the job creation, retries after some time (define this time), attempts to acquire lock again and succeeds.
3. instance 2 creates job successfully

#### One instance crashes while acquiring lock, and reboots afterwards

1. instance 1 acquires lock, but crashes
2. instance 1 reboots
3. instance 1 has to check database for locks that has in progress state, but have no job created for it

## Tables

Crons:

- id: str uuid
- customer id: id of the customer who this cron belongs to
- job name: unique str
- group: str
- url: str
- method: str
- timeout: double
- cron expression: str
- cron timezone: str
- created at: timestamp
- updated at: timestamp

Cron Executions

- id: str uuid
- cron id: str uuid foreign key
- status: str
- error: str

Alerts configuration:

- id: str uuid
- job id: str uuid
- email: str
- amount of failures: int

## Features

- I want to create/edit/remove any amount of cronjobs to call my custom API, in a custom route, with a custom timeout
  - setup database to be used in development
  - [spike] define database migration process
  - [spike] define ORM (database first recommended)
  - [spike] define model/structure for cronjobs
  - [spike] define server routes/features, as well as documentation  strategy for the API
  - [spike] define logging tool, and good practices for loggin in this application
  - [spike] define initial metrics (observability) and tools to be used
  - [spike] define tracing (observability) tools to be used
- Given that I created cronjobs, I want my configured request to be executed, respecting my cron expression
  - [spike] investigate cronjobs locking for request jobs (cloud scenario with lots of instances)
  - create Cron service, that will, CRUD cronjobs and execute them
- I want to be able to list and filter cronjobs that I created
- Given that I created a cronjob previously I want to be able to pause it's execution until I turn it back on again
- Given a cronjob previously created, I want to check the jobs execution history
- I want to create 1 or more alerts, associated with a cronjob, that sends me an email, when the cronjob API call fails given an alert configuration
- As a user, I want to login into my account and see only my cronjobs, and not from other users
