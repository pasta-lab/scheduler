ENV?=dev
TAG?=$(shell git rev-parse --short HEAD)

define migrate_database
	$(eval $(call generate_dsn,dsn,$(1)))
	migrate -database ${dsn} -source "file://migrations" $(2)
endef

define generate_dsn
	dsn="postgresql://usr:pwd@localhost:5432/$(2)?sslmode=disable"
	$(1) := $$(dsn)
endef

.PHONY: install-tools
install-tools:
	make -C ./tools/

.PHONY: c4-generate
c4-generate:
	stz gen gitlab.com/pasta-lab/scheduler/docs/arch -out build/c4-workspace.json

.PHONY: c4-docs
c4-docs: c4-generate
	docker run -it --rm -p 8000:8080 -v "${PWD}/build/c4-workspace.json:/usr/local/structurizr/workspace.json" structurizr/lite

.PHONY: run
run: tidy
	go run ./cmd/server/main.go

.PHONY: run-watch
run-watch:
	reflex -d none -s -r '\.go$$' make run

.PHONY: build
build:
	go build -ldflags "-X main.Version=${TAG}" -o ./build/server ./cmd/server/main.go

.PHONY: test
test:
	go test ./...

.PHONY: test-watch
test-watch:
	reflex -d none -s -r '\.go$$' make test

.PHONY: tidy
tidy:
	go mod tidy

.PHONY: psql
psql:
	$(eval $(call generate_dsn,dsn,"scheduler"))
	docker-compose exec db /bin/sh -c "psql ${dsn}"

.PHONY: reset-db
reset-db:
	docker-compose down -v

.PHONY: migrate-up
migrate-up:
	$(call migrate_database,"scheduler","up")

.PHONY: migrate-down
migrate-down:
	$(call migrate_database,"scheduler","down")

.PHONY: generate-models
generate-models: migrate-up
	sqlboiler psql -c sqlboiler.toml

.PHONY: k8s
k8s:
	k3d cluster create dev --port 8080:80@loadbalancer --port 8443:443@loadbalancer
