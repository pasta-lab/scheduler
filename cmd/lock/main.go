package main

import (
	"database/sql"
	"errors"
	"log"
	"time"

	_ "github.com/lib/pq"
	"github.com/robfig/cron/v3"
	"github.com/volatiletech/sqlboiler/v4/queries/qm"
	"gitlab.com/pasta-lab/scheduler/internal/model"
	"golang.org/x/net/context"
)

const connURL = "postgresql://usr:pwd@localhost:5432/scheduler?sslmode=disable"

type jobCreator struct {
	db *sql.DB
	id string
}

func (j jobCreator) Run() {
	ctx := context.Background()
	tx, err := j.db.BeginTx(ctx, nil)
	if err != nil {
		log.Printf("failed to begin transaction: %v", err)
		return
	}
	acquired, err := j.tryAcquireLock(ctx, tx, "aaaa")
	if err != nil {
		log.Printf("failed while acquiring lock: %v", err)
		return
	}
	if acquired {
		log.Printf("sID=%v, acquired lock? %#v", j.id, acquired)
	}
	time.Sleep(time.Duration(5) * time.Second)
	defer tx.Commit()
}

func (j jobCreator) tryAcquireLock(ctx context.Context, tx *sql.Tx, cronID string) (bool, error) {
	var lock model.CronExecutionLock
	err := model.CronExecutionLocks(model.CronExecutionLockWhere.CronID.EQ(cronID), qm.For("UPDATE SKIP LOCKED")).Bind(ctx, tx, &lock)
	if err != nil {
		_ = tx.Rollback()

		if errors.Is(err, sql.ErrNoRows) {
			return false, nil
		}
		return false, err
	}

	return true, nil
}

/*
You need to insert some rows into the database before running this POC, check the migration files.

This POC uses row level locking to acquire distributed locks
*/
func main() {
	db, err := sql.Open("postgres", connURL)
	if err != nil {
		log.Fatalf("failed to connect to DB: %v", err)
	}
	defer db.Close()

	c := cron.New()
	j1 := jobCreator{id: "1", db: db}
	_, err = c.AddJob("*/1 * * * *", j1)
	if err != nil {
		log.Fatalf("failed to add job: %v", err)
	}

	j2 := jobCreator{id: "2", db: db}
	_, err = c.AddJob("*/1 * * * *", j2)
	if err != nil {
		log.Fatalf("failed to add job: %v", err)
	}
	j3 := jobCreator{id: "3", db: db}
	_, err = c.AddJob("*/1 * * * *", j3)
	if err != nil {
		log.Fatalf("failed to add job: %v", err)
	}
	j4 := jobCreator{id: "4", db: db}
	_, err = c.AddJob("*/1 * * * *", j4)
	if err != nil {
		log.Fatalf("failed to add job: %v", err)
	}
	j5 := jobCreator{id: "5", db: db}
	_, err = c.AddJob("*/1 * * * *", j5)
	if err != nil {
		log.Fatalf("failed to add job: %v", err)
	}

	c.Run()
}
